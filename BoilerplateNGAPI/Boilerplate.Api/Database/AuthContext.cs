﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Boilerplate.Api.Database
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext()
            : base("AuthContext")
        {

        }
    }
}