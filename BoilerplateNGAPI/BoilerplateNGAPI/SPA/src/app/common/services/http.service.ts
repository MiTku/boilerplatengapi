import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { MainSettings} from '../configs/main.config';

@Injectable()
export class HttpService {

  constructor(private http: Http) { }

  // 
  getData(suffix){
    var observable =  this.http.get(`${MainSettings.API_ENDPOINT}/${suffix}`);
    return observable;
  }
}
