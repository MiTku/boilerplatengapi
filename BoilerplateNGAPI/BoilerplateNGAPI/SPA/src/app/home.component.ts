import { Component, OnInit } from '@angular/core';
import { HttpService } from './common/services/http.service';
import { Response } from '@angular/http';
@Component({
  selector: 'aping-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  constructor(private http: HttpService) { }

  ngOnInit() {
    var data = this.http.getData('').subscribe((data: Response)=> console.log(data));
  }
}
