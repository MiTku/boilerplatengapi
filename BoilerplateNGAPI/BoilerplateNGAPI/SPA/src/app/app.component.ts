import { Component } from '@angular/core';

@Component({
  selector: 'aping-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'aping works!';
}
