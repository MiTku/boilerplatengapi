import { browser, element, by } from 'protractor';

export class BoilerplateNGAPIPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('aping-root h1')).getText();
  }
}
