import { BoilerplateNGAPIPage } from './app.po';

describe('boilerplate-ngapi App', function() {
  let page: BoilerplateNGAPIPage;

  beforeEach(() => {
    page = new BoilerplateNGAPIPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('aping works!');
  });
});
